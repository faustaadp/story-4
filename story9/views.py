from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
# Create your views here.
def story9(request):
	return render(request, 'story9/story9.html')

def register(request):
	if request.method == "POST":
		username = request.POST.get('username')
		password = request.POST.get('password')
		if User.objects.filter(username = username).exists():
			messages.error(request, 'Username sudah ada')
			return redirect('story9:register')
		user = User.objects.create_user(username = username)
		user.set_password(password)
		user.save()
		return redirect('story9:login')
	return render(request, 'story9/register.html')

def log_in(request):
	if request.method == "POST":
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username = username, password = password)
		if user is not None:
			if user.is_active:
				login(request, user)	
			return redirect('story9:story9')
		else:
			messages.error(request, 'Username / Password salah')
			return redirect('story9:login')
	return render(request, 'story9/login.html')

def log_out(request):
	logout(request)
	return redirect('story9:story9')