from django.test import TestCase, SimpleTestCase, Client, LiveServerTestCase
from django.contrib.auth.models import User
from .views import story9, register, log_in, log_out
from django.urls import resolve

# Create your tests here.

class MainTestCase(TestCase):
	def test_url_story9(self):
		response = self.client.get('/story9/')
		self.assertEqual(response.status_code, 200)

	def test_url_register(self):
		response = self.client.get('/story9/register/')
		self.assertEqual(response.status_code, 200)

	def test_url_login(self):
		response = self.client.get('/story9/login/')
		self.assertEqual(response.status_code, 200)

	def test_url_logout(self):
		response = self.client.get('/story9/logout/')
		self.assertEqual(response.status_code, 302)

	def test_register(self):
		response = self.client.post('/story9/register/' , data={'username' : 'user01', 'password' : 'pass01'})
		self.assertIsNotNone(User.objects.get(username='user01'))
		self.assertEqual(response.status_code, 302)

	def test_register_udah_ada(self):
		self.client.post('/story9/register/' , data={'username' : 'user01', 'password' : 'pass01'})
		self.client.post('/story9/register/' , data={'username' : 'user01', 'password' : 'pass01'})
		response = self.client.get('/story9/register/')
		html_kembalian = response.content.decode('utf8')
		self.assertIn("Username sudah ada", html_kembalian)
		self.assertEqual(response.status_code, 200)

	def test_login(self):
		self.client.post('/story9/register/' , data={'username' : 'user01', 'password' : 'pass01'})
		response = self.client.post('/story9/login/' , data={'username' : 'user01', 'password' : 'pass01'})	
		self.assertEqual(response.status_code, 302)

	def test_login_salah(self):
		self.client.post('/story9/register/' , data={'username' : 'user01', 'password' : 'pass01'})
		response = self.client.post('/story9/login/' , data={'username' : 'user01', 'password' : 'pass02'})	
		response = self.client.get('/story9/login/')
		html_kembalian = response.content.decode('utf8')
		self.assertIn("Username / Password salah", html_kembalian)
		self.assertEqual(response.status_code, 200)