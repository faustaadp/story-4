function tukar(id1, id2) {
    var isi1 = document.getElementById(id1);
    var isi2 = document.getElementById(id2);
    var temp = isi1.innerHTML;
    isi1.innerHTML = isi2.innerHTML;
    isi2.innerHTML = temp;
}

function tukaratas(pos) {
    if(pos == 1)return ;
    var atas = pos - 1;
    tukar(`judul-${atas}`, `judul-${pos}`); 
    tukar(`isi-${atas}`, `isi-${pos}`);
}

function tukarbawah(pos) {
    if(pos == 4)return ;
    var bawah = pos + 1;
    tukar(`judul-${bawah}`, `judul-${pos}`);
    tukar(`isi-${bawah}`, `isi-${pos}`);
}

$(".accordion-btn").click(function() {
    var bodyId = $(this).attr('data-body');
    $(`#${bodyId}`).slideToggle("slow");
});