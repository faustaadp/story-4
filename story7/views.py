from django.shortcuts import render

def story7(request):
	context = {
		'contents': [
			('Tentang Saya', ['Raden Fausta Anugrah Dianparama', 'Mahasiswa Fasilkom UI']),
			('Pengalaman', ['Scientific Committee Pelatnas 1 2020', 'Scientific Committee CPC Compfest 12', 'Tester APIO 2020']),
			('Pendidikan', ['SD Negeri Lempuyangwangi', 'SMP Negeri 1 Yogyakarta', 'SMA Negeri 1 Yogyakarta', 'Universitas Indonesia']),
			('Prestasi', ['Medali Perak IOI 2019 (Azerbaijan)', 'Medali Perunggu APIO 2019 (Russia)', 'Medali Emas NOI 2019 (Singapore)', 'Medali Perak OSN 2017 (Indonesia)']),
		]
	}
	return render(request, 'story7/story7.html', context)