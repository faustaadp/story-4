from django.test import TestCase
import json
# Create your tests here.

class MainTestCase(TestCase):
	def test_url(self):
		response = self.client.get('/story8/')
		self.assertEqual(response.status_code, 200)

	def test_data_buku(self):
		response = self.client.get('/story8/data/?q=purcell')
		response_json = json.loads(response.content.decode())
		self.assertEqual(response.status_code, 200)
		self.assertNotEquals(response_json, {'totalItems': 0})


