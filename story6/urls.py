from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('kegiatan/', views.kegiatan, name='kegiatan'),
    path('tambah_kegiatan/', views.tambah_kegiatan, name='tambah_kegiatan'),
    path('detail_kegiatan/<int:nomor>', views.detail_kegiatan, name='detail_kegiatan'),
    path('hapus_kegiatan/<int:nomor>', views.hapus_kegiatan, name='hapus_kegiatan'),
    path('tambah_peserta/<int:nomor>', views.tambah_peserta, name='tambah_peserta'),
    path('hapus_peserta/<int:nomor>', views.hapus_peserta, name='hapus_peserta'),
]
