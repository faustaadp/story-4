from django import forms
from .models import Kegiatan, Peserta

class FormKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['nama', 'deskripsi']
        widgets = {
            'nama' : forms.TextInput(attrs={'class': 'form-control'}),
            'deskripsi' : forms.TextInput(attrs={'class': 'form-control'}),
        }

class FormPeserta(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ['nama']
        widgets = {
            'nama' : forms.TextInput(attrs={'class': 'form-control'}),
        }


