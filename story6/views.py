from django.shortcuts import render, redirect
from .models import Kegiatan, Peserta
from .forms import FormKegiatan, FormPeserta
# Create your views here.
def kegiatan(request):
	kegiatan = Kegiatan.objects.all()
	peserta = Peserta.objects.all()
	context = {'kegiatan': kegiatan, 'peserta': peserta}
	return render(request, 'story6/kegiatan.html', context)

def tambah_kegiatan(request):
	form = FormKegiatan()
	if request.method == "POST":
		form = FormKegiatan(request.POST)
		if form.is_valid():
			form.save()
			return redirect('story6:kegiatan')
	context = {'form' : form}
	return render(request, 'story6/tambah_kegiatan.html', context)

def detail_kegiatan(request, nomor):
	kegiatan = Kegiatan.objects.get(id = nomor)
	peserta = Peserta.objects.all()
	context = {'kegiatan': kegiatan, 'peserta': peserta}
	return render(request, 'story6/detail_kegiatan.html', context)

def hapus_kegiatan(request, nomor):
	kegiatan = Kegiatan.objects.get(id = nomor)
	kegiatan.delete()
	return redirect('story6:kegiatan')

def tambah_peserta(request, nomor):
	form = FormPeserta()
	if request.method == "POST":
		form = FormPeserta(request.POST)
		if form.is_valid():
			peserta = Peserta(nama=form.data['nama'], kegiatan = Kegiatan.objects.get(id = nomor))
			peserta.save()
			return redirect('story6:kegiatan')
	context = {'form' : form}
	return render(request, 'story6/tambah_peserta.html', context)

def hapus_peserta(request, nomor):
	peserta = Peserta.objects.get(id = nomor)
	id = peserta.kegiatan.id
	peserta.delete()
	return redirect('/detail_kegiatan/' + str(id))