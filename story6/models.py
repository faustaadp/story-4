from django.db import models

# Create your models here.

class Kegiatan(models.Model):
	nama = models.CharField(max_length = 50)
	deskripsi = models.CharField(max_length = 50)

class Peserta(models.Model):
	nama = models.CharField(max_length = 50)
	kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
