from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from .models import Kegiatan, Peserta
from .forms import FormKegiatan, FormPeserta

class MainTestCase(TestCase):
	def test_url_kegiatan(self):
		response = self.client.get('/kegiatan/')
		self.assertEqual(response.status_code, 200)

	def test_url_tambah_kegiatan(self):
		response = self.client.get('/tambah_kegiatan/')
		self.assertEqual(response.status_code, 200)

	def test_url_detail_kegiatan(self):
		kegiatan = Kegiatan.objects.create(nama="Main", deskripsi="Main bareng")
		id = kegiatan.id
		response = Client().get('/detail_kegiatan/' + str(id))
		self.assertEqual(response.status_code, 200)

	def test_url_hapus_kegiatan(self):
		kegiatan = Kegiatan.objects.create(nama="Main", deskripsi="Main bareng")
		id = kegiatan.id
		response = Client().get('/hapus_kegiatan/' + str(id))
		self.assertEqual(response.status_code, 302)

	def test_url_tambah_peserta(self):
		kegiatan = Kegiatan.objects.create(nama="Main", deskripsi="Main bareng")
		peserta = Peserta.objects.create(nama="Fausta", kegiatan=kegiatan)
		id = peserta.id
		response = Client().get('/tambah_peserta/' + str(id))
		self.assertEqual(response.status_code, 200)

	def test_url_hapus_peserta(self):
		kegiatan = Kegiatan.objects.create(nama="Main", deskripsi="Main bareng")
		peserta = Peserta.objects.create(nama="Fausta", kegiatan=kegiatan)
		id = peserta.id
		response = Client().get('/hapus_peserta/' + str(id))
		self.assertEqual(response.status_code, 302)

	def test_template_kegiatan(self):
		response = Client().get('/kegiatan/')
		self.assertTemplateUsed(response, 'main/base.html')

	def test_template_tambah_kegiatan(self):
		response = Client().get('/tambah_kegiatan/')
		self.assertTemplateUsed(response, 'main/base.html')

	def test_model_buat(self):
		kegiatan = Kegiatan.objects.create(nama="Main", deskripsi="Main bareng")
		self.assertEqual(Kegiatan.objects.all().count(), 1)

	def test_post_kegiatan(self):
		response = self.client.post("/tambah_kegiatan/", data={"nama": "Main", "deskripsi": "Main bareng"})
		self.assertEqual(response.status_code, 302)

	def test_hapus_kegiatan(self):
		kegiatan = Kegiatan.objects.create(nama="Main", deskripsi="Main bareng")
		kegiatan.delete()
		self.assertEqual(Kegiatan.objects.all().count(), 0)

	def test_post_anggota(self):
		kegiatan = Kegiatan.objects.create(nama="Main", deskripsi="Main bareng")
		response = self.client.post("/tambah_peserta/" + str(kegiatan.id), data={"nama": "Main", "kegiatan": kegiatan})
		self.assertEqual(response.status_code, 302)



