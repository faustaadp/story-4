from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('index/', views.home, name='home'),
    path('biodata/', views.biodata, name='biodata'),
    path('pengalaman/', views.pengalaman, name='pengalaman'),
    path('pendidikan/', views.pendidikan, name='pendidikan'),
    path('story1/', views.story1, name='story1'),
    path('jadwal/', views.jadwal, name='jadwal'),
    path('tambah/', views.tambah, name='tambah'),
    path('hapus/<int:nomor>', views.hapus, name='hapus'),
    path('detail/<int:nomor>', views.detail, name='detail'),
]
