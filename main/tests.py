from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from .models import Jadwal
from .forms import FormJadwal

class MainTestCase(TestCase):
	def test_url_index(self):
		response = self.client.get('/index/')
		self.assertEqual(response.status_code, 200)

	def test_url_biodata(self):
		response = self.client.get('/biodata/')
		self.assertEqual(response.status_code, 200)

	def test_url_pengalaman(self):
		response = self.client.get('/pengalaman/')
		self.assertEqual(response.status_code, 200)

	def test_url_pendidikan(self):
		response = self.client.get('/pendidikan/')
		self.assertEqual(response.status_code, 200)

	def test_url_story1(self):
		response = self.client.get('/story1/')
		self.assertEqual(response.status_code, 200)

	def test_url_jadwal(self):
		response = self.client.get('/jadwal/')
		self.assertEqual(response.status_code, 200)

	def test_url_tambah(self):
		response = self.client.get('/tambah/')
		self.assertEqual(response.status_code, 200)

	def test_url_detail_jadwal(self):
		jadwal = Jadwal.objects.create(nama="PPW", sks=3, dosen="Bu Iis", semester="Ganjil", tahun="2020/2021", ruang="Zoom", deskripsi="Perancangan dan Pemrograman Web")
		id = jadwal.id
		response = Client().get('/detail/' + str(id))
		self.assertEqual(response.status_code, 200)

	def test_url_hapus_jadwal(self):
		jadwal = Jadwal.objects.create(nama="PPW", sks=3, dosen="Bu Iis", semester="Ganjil", tahun="2020/2021", ruang="Zoom", deskripsi="Perancangan dan Pemrograman Web")
		id = jadwal.id
		response = Client().get('/hapus/' + str(id))
		self.assertEqual(response.status_code, 302)

	def test_post_jadwal(self):
		response = self.client.post("/tambah/", data={"nama":"PPW", "sks":3, "dosen":"Bu Iis", "semester":"Ganjil", "tahun":"2020/2021", "ruang":"Zoom", "deskripsi":"Perancangan dan Pemrograman Web"})
		self.assertEqual(response.status_code, 302)


