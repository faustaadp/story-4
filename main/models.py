from django.db import models

# Create your models here.

class Jadwal(models.Model):
	nama = models.CharField(max_length = 50)
	dosen = models.CharField(max_length = 50)
	sks = models.PositiveIntegerField(default = 1)
	deskripsi = models.CharField(max_length = 100)
	semester = models.CharField(max_length = 6, choices= [('Ganjil', 'Ganjil'), ('Genap', 'Genap')], default = ('Ganjil', 'Ganjil'));
	tahun = models.CharField(max_length = 9, choices= [('2019/2020', '2019/2020'), ('2020/2021', '2020/2021'), ('2021/2022', '2021/2022'), ('2022/2023', '2022/2023')], default = ('2020/2021', '2020/2021'));
	ruang = models.CharField(max_length = 50)
