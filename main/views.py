from django.shortcuts import render, redirect
from .models import Jadwal
from .forms import FormJadwal

def home(request):
	return render(request, 'main/index.html')

def biodata(request):
	return render(request, 'main/biodata.html')

def pengalaman(request):
	return render(request, 'main/pengalaman.html')

def pendidikan(request):
	return render(request, 'main/pendidikan.html')

def story1(request):
	return render(request, 'main/story1.html')

def jadwal(request):
	jadwal = Jadwal.objects.all()
	context = {'jadwal': jadwal}
	return render(request, 'main/jadwal.html', context)

def tambah(request):
	form = FormJadwal()
	if request.method == "POST":
		form = FormJadwal(request.POST)
		if form.is_valid():
			form.save()
			return redirect('main:jadwal')
	context = {'form' : form}
	return render(request, 'main/tambah.html', context)

def hapus(request, nomor):
	jadwal = Jadwal.objects.get(id = nomor)
	jadwal.delete()
	return redirect('main:jadwal')

def detail(request, nomor):
	jadwal = Jadwal.objects.get(id = nomor)
	context = {'jadwal': jadwal}
	return render(request, 'main/detail.html', context)

