from django import forms
from .models import Jadwal

class FormJadwal(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = ['nama', 'dosen', 'sks', 'deskripsi', 'semester', 'tahun', 'ruang']
        widgets = {
            'nama' : forms.TextInput(attrs={'class': 'form-control'}),
            'dosen' : forms.TextInput(attrs={'class': 'form-control'}),
            'sks' : forms.NumberInput(attrs={'class': 'form-control'}),
            'deskripsi' : forms.TextInput(attrs={'class': 'form-control'}),
            'semester' : forms.Select(attrs={'class': 'form-control'}),
            'tahun' : forms.Select(attrs={'class': 'form-control'}),
            'ruang' : forms.TextInput(attrs={'class': 'form-control'}),
        }

